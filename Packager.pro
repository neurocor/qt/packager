CMN_DIR = ../common

include ($$CMN_DIR/zip/zlib.pri)
include ($$CMN_DIR/json/json.pri)


INCLUDEPATH += $$CMN_DIR
DEPENDPATH += $$CMN_DIR

DESTDIR = ../$$TARGET

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    customlineedit.cpp \
    main.cpp \
    options.cpp \
    widget.cpp

HEADERS += \
    customlineedit.h \
    options.h \
    widget.h

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
