#ifndef OPTIONS_H
#define OPTIONS_H


#include <QString>
#include <QStringList>
#include <QSize>

class QJsonObject;


/*!
 * \brief Пути к файлам программы
 */
struct Paths
{
    /*!
     * \brief Инициализация файлов программы
     */
    static void init();

    /*!
     * \brief Получить полный путь к файлу и директории
     * \param key ключ файла или директории
     * \return полный путь
     */
    static QString get(const QString &key);


    static QString options;			//!< файл сохраненных общих настроек программы
    static QString docsDir;
    static QString installDir;
    static QString setupDir;
    static QString sourceDir;
    static QString testingDir;
};




class OptionsSaver
{
public:
    OptionsSaver();
    ~OptionsSaver();
};

class GeneralOptions
{
public:

    static inline GeneralOptions &global(){
        static GeneralOptions opt;
        return opt;
    }

    void init();

    QJsonObject save() const;
    void load(const QJsonObject &json);

    QString packName;
    QString packVersion;
    QString packDirPath;
    QString buildDirPath;
    QStringList srcDirPaths;
    QString innoFilePath;
    QStringList filters;


private:
    explicit GeneralOptions();
};

#endif // OPTIONS_H
