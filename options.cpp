#include "options.h"

#include <QApplication>
#include <QJsonObject>
#include <QJsonArray>

#include "json/jsonsaver.h"

QString Paths::options = "setup/options.json";
QString Paths::docsDir = "docs";
QString Paths::installDir = "install";
QString Paths::setupDir = "setup";
QString Paths::sourceDir = "source";
QString Paths::testingDir = "testing";



void Paths::init()
{

}

QString Paths::get(const QString &key)
{
    return QApplication::applicationDirPath() + '/' + key;
}


OptionsSaver::OptionsSaver()
{
    Paths::init();

    QJsonObject json;
    if (Paths::get(Paths::options).contains(".json") && JsonSaver::load(json, Paths::get(Paths::options)))
    {
        GeneralOptions::global().load(json["GeneralOptions"].toObject());
    }


}

OptionsSaver::~OptionsSaver()
{
    if (Paths::get(Paths::options).contains(".json"))
    {
        QJsonObject json;
        json["GeneralOptions"] = GeneralOptions::global().save();

        JsonSaver::save(json, Paths::get(Paths::options));
    }
}


GeneralOptions::GeneralOptions()
{
    init();
}

void GeneralOptions::init()
{
    srcDirPaths.clear();
    filters.clear();
}

QJsonObject GeneralOptions::save() const
{

    QJsonObject json;

    json["buildDirPath"] = buildDirPath;
    json["packName"] = packName;
    json["packVersion"] = packVersion;
    json["packDirPath"] = packDirPath;
    json["innoFilePath"] = innoFilePath;

    QJsonArray filtObjArr;

    for(const auto &filter : filters){
        filtObjArr.append(filter);
    }

    json["filters"] = filtObjArr;


    QJsonArray srcDirPathsObjArr;

    for(const auto &srcDir : srcDirPaths){
        srcDirPathsObjArr.append(srcDir);
    }


    json["srcDirPaths"] = srcDirPathsObjArr;

    return json;
}

void GeneralOptions::load(const QJsonObject &json)
{
    buildDirPath = json["buildDirPath"].toString();
    packName = json["packName"].toString();
    packVersion = json["packVersion"].toString();
    packDirPath = json["packDirPath"].toString();
    innoFilePath = json["innoFilePath"].toString();


    filters.clear();
    srcDirPaths.clear();

    for(const auto &filtObj : json["filters"].toArray()){
        filters.append(filtObj.toString());
    }

    for(const auto &srcObj : json["srcDirPaths"].toArray()){
        srcDirPaths.append(srcObj.toString());
    }



}
