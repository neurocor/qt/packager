#include "widget.h"

#include <QApplication>

#include "options.h"

int main(int argc, char *argv[])
{

    QApplication a(argc, argv);

    OptionsSaver saver;
    Widget w;
    w.show();
    return a.exec();
}
