#include "widget.h"

#include <QApplication>
#include <QFile>
#include <QFileInfo>
#include <QFileInfoList>
#include <QDir>
#include <QFileDialog>


#include <QFormLayout>

#include <QLineEdit>
#include <QPushButton>
#include <QListWidget>

#include <QDir>
#include <QFileDialog>

#include "zip/qzipwriter.h"
#include "json/jsonsaver.h"

#include "options.h"
#include "customlineedit.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
{


    setMinimumSize(640, 380);

    auto mainLay = new QGridLayout(this);

    auto gridSrclay = new QGridLayout(this);

    auto packNameLE = new QLineEdit(this);
    auto packVersionLE = new QLineEdit(this);

    // auto srcPathLE = new QLineEdit(this);
    // auto srcPathBtn= new QPushButton("...",this);




    auto srcListWidget = new QListWidget(this);
    auto srcPathAddBtn= new QPushButton("Add",this);
    auto srcPathRemoveBtn= new QPushButton("Remove",this);

    auto buildPathLE = new QLineEdit(this);
    auto buildPathBtn= new QPushButton("...",this);

    auto outPathLE = new QLineEdit(this);
    auto outPathBtn= new QPushButton("...",this);

    auto innoPathLE= new QLineEdit(this);
    auto innoPathBtn= new QPushButton("...",this);

    auto packageBtn= new QPushButton("Package");

    packNameLE->setPlaceholderText("App name");
    packVersionLE->setPlaceholderText("App version");
    buildPathLE->setPlaceholderText("Build dir");
    outPathLE->setPlaceholderText("Out dir");
    innoPathLE->setPlaceholderText("Inno setup file");


    gridSrclay->addWidget(srcListWidget,0,0,2,1);
    gridSrclay->addWidget(srcPathAddBtn,0,1);
    gridSrclay->addWidget(srcPathRemoveBtn,1,1);


    mainLay->addWidget(packNameLE,0,0);
    mainLay->addWidget(packVersionLE,0,1);
    mainLay->addLayout(gridSrclay,1,0,1,2);
    mainLay->addWidget(buildPathLE,2,0);
    mainLay->addWidget(buildPathBtn,2,1);
    mainLay->addWidget(outPathLE,3,0);
    mainLay->addWidget(outPathBtn,3,1);
    mainLay->addWidget(innoPathLE,4,0);
    mainLay->addWidget(innoPathBtn,4,1);
    mainLay->addWidget(packageBtn,5,0,1,2);



    connect(srcPathAddBtn, &QPushButton::clicked,[this, srcListWidget]{
        auto dirPath = QFileDialog::getExistingDirectory(this, "Select src dir");

        if(dirPath.isEmpty())
            return;

        srcListWidget->addItem(dirPath.replace('/','\\'));


    });

    connect(srcPathRemoveBtn, &QPushButton::clicked,[this, srcListWidget]{

        int curRow = srcListWidget->currentRow();
        if(auto curItem = srcListWidget->currentItem()){

            srcListWidget->removeItemWidget(curItem);
        }

    });

    connect(buildPathBtn, &QPushButton::clicked,[this, buildPathLE]{
        auto dirPath = QFileDialog::getExistingDirectory(this, "Select build dir", GeneralOptions::global().buildDirPath);

        if(dirPath.isEmpty())
            return;

        buildPathLE->setText(dirPath.replace('/','\\'));
    });

    connect(outPathBtn, &QPushButton::clicked,[this, outPathLE]{
        auto dirPath = QFileDialog::getExistingDirectory(this, "Select out dir", GeneralOptions::global().packDirPath);

        if(dirPath.isEmpty())
            return;

        outPathLE->setText(dirPath.replace('/','\\'));

    });

    connect(innoPathBtn, &QPushButton::clicked,[this, innoPathLE]{
        auto dirPath = QFileDialog::getOpenFileName(this, "Select inno file", QFileInfo(GeneralOptions::global().innoFilePath).filePath(), "ISCC.exe");

        if(dirPath.isEmpty())
            return;

        innoPathLE->setText(dirPath.replace('/','\\'));

    });
    connect(packNameLE, &QLineEdit::textChanged,[](const QString &text){
        GeneralOptions::global().packName = text;
    });
    connect(packVersionLE, &QLineEdit::textChanged,[](const QString &text){
        GeneralOptions::global().packVersion = text;
    });
    connect(buildPathLE, &QLineEdit::textChanged,[](const QString &text){
        GeneralOptions::global().buildDirPath = text;
    });
    connect(outPathLE, &QLineEdit::textChanged,[](const QString &text){
        GeneralOptions::global().packDirPath = text;
    });
    connect(innoPathLE, &QLineEdit::textChanged,[](const QString &text){
        GeneralOptions::global().innoFilePath = text;
    });


    connect(packageBtn, &QPushButton::clicked,[]{

        auto opt = GeneralOptions::global();


        QDir sourceDir(opt.packDirPath + '\\' + opt.packVersion + '\\' + Paths::sourceDir);

        for(const auto &srcDir : opt.srcDirPaths){

            auto zipName = sourceDir.path() + '\\' + QDir(srcDir).dirName() + ".zip";

            QZipWriter writer(zipName);

            auto content = QDir(srcDir).entryInfoList(QDir::NoDotAndDotDot | QDir::Files | QDir::Dirs | QDir::Readable);

            for(const auto &contFile : content){
                for(const auto &filter : opt.filters){
                    if(contFile.isFile() && contFile.fileName().contains(filter))
                        return;
                }

                QFile tmpFile(contFile.absoluteFilePath());
                if(tmpFile.open(QFile::ReadOnly)){
                    writer.addFile(contFile.dir().dirName() + '\\' + contFile.fileName(), tmpFile.readAll());
                    tmpFile.close();
                }
            }

            writer.close();
        }


        auto outFileName = sourceDir.path() + '\\' + opt.packName + "-source-" + opt.packVersion + ".zip";

        if(QFile::exists(outFileName))
            QFile::remove(outFileName);


        auto content = sourceDir.entryInfoList(QStringList()<<"*.zip", QDir::NoDotAndDotDot | QDir::Files | QDir::Readable);

        QZipWriter writer(outFileName);

        for(const auto &contFile : content){

            QFile tmpFile(contFile.absoluteFilePath());

            if(tmpFile.open(QFile::ReadOnly)){

                writer.addFile(contFile.fileName(), tmpFile.readAll());
                tmpFile.close();
                tmpFile.remove();
            }
        }


    });



    auto &opt = GeneralOptions::global();

    buildPathLE->setText(opt.buildDirPath);
    outPathLE->setText(opt.packDirPath);
    packNameLE->setText(opt.packName);
    packVersionLE->setText(opt.packVersion);
    innoPathLE->setText(opt.innoFilePath);


    srcListWidget->clear();

    srcListWidget->addItems(opt.srcDirPaths);

}

Widget::~Widget()
{


}
